﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using my_tdd_application_api.Interfaces;
using my_tdd_application_api.Models;

namespace my_tdd_application_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private readonly ILogger<CourseController> _logger;
        private readonly ICourseService _courseService;

        public CourseController(ILogger<CourseController> logger, ICourseService courseService)
        {
            _logger = logger;
            _courseService = courseService;
        }

        [HttpGet]
        public IEnumerable<Course> GetAllCourse()
        {
            return _courseService.returnAllCourses();
        }

        [HttpPost]
        public string AddCourse(Course myCourse)
        {
            var added = _courseService.AddCourse(myCourse);
            if (added) { return "Course is added"; }
            else { return "Invalid Course to add"; }
        }
    }
}
