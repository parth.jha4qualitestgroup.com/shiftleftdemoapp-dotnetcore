﻿using my_tdd_application_api.Interfaces;
using my_tdd_application_api.Models;
using System;
using System.Collections.Generic;

namespace my_tdd_application_api.Services
{
    public class UserService : IUserService
    {
        List<User> _userItems;
        private readonly IRoleService _roleService;
        private readonly ICourseService _courseService;

        //UserService will also extend Data Layer which has all DB Queries
        public UserService(IRoleService roleService, ICourseService courseService)
        {
            _userItems = new List<User>(){
            new User(){userId=1001,userName="myFirstUser",description="userDescription",userRole=new Roles(){roleId=101,roleName="myFirstRoleName"}}
        };
            _roleService = roleService;
            _courseService = courseService;
        }

        public List<User> returnAllUserList()
        {
            //return _data.GetAllUser();
            return _userItems;
        }

        public User returnUserDetailsforUserId(int userId)
        {
            //return _data.GetUser(userId)
            //return _userItems.Find(x => x.userId.Equals(userId));
            var myUser = _userItems.Find(x => x.userId.Equals(userId));
            if (myUser == null) { return new User() { userId = 0000, userName = "InvalidUserId" }; }
            else { return myUser; }
        }

        public bool AddUserToList(User myUser)
        {
            if (String.IsNullOrEmpty(myUser.userName) || String.IsNullOrEmpty(myUser.description) || myUser.userId <= 1000)
            { return false; }

            //_userItems = _data.GetAllUser();
            var invalidUser = false;
            for (int ui = 0; ui < _userItems.Count; ui++) { if (_userItems[ui].userId == myUser.userId) { invalidUser = true; } };
            if (invalidUser) { return false; }
            //_data.AddUser(myUser);
            else { _userItems.Add(myUser); return true; }
        }

        public bool updateUserRole(int userId, int roleId)
        {
            var updated = false;
            //_userItems = _data.GetAllUser();
            int userIndex = -1;
            for (int i = 0; i < _userItems.Count; i++) { if (_userItems[i].userId == userId) { userIndex = i; } };

            List<Roles> rolelist = _roleService.returnAllRoleList();
            int roleIndex = -1;
            for (int i = 0; i < rolelist.Count; i++) { if (rolelist[i].roleId == roleId) { roleIndex = i; } };

            if (roleIndex == -1)
            {
                var myRole = new Roles() { roleId = roleId, roleName = roleId.ToString() };
                _roleService.AddRoleToList(myRole);
                rolelist = _roleService.returnAllRoleList();
                for (int i = 0; i < rolelist.Count; i++) { if (rolelist[i].roleId == roleId) { roleIndex = i; } };
            }

            if (userIndex > -1 && roleIndex > -1)
            {
                _userItems[userIndex].userRole = rolelist[roleIndex];
                updated = true;
            }
            return updated;
        }

        public bool updateUserCourse(int userId)
        {
            bool updated = false;
            List<Course> courselist;
            int userIndex = -1;
            int userRoleId = -1;
            for (int i = 0; i < _userItems.Count; i++) { if (_userItems[i].userId == userId) { userIndex = i; userRoleId = _userItems[i].userRole.roleId; } };
            courselist = _courseService.returnAllCourses();
            if (userIndex > -1)
            {
                int courseIndex = -1;
                for (int i = 0; i < courselist.Count; i++) { if (courselist[i].cId == userRoleId) { courseIndex = i; } };
                if (courseIndex == -1)
                {
                    //_userItems[userIndex].course = courselist[0];
                    var myCourse = new Course() { cId = userRoleId, courseDescription = userRoleId.ToString() };
                    _courseService.AddCourse(myCourse);
                    courselist = _courseService.returnAllCourses();
                    for (int i = 0; i < courselist.Count; i++) { if (courselist[i].cId == userRoleId) { courseIndex = i; } };
                    _userItems[userIndex].course = courselist[courseIndex];
                    updated = true;
                }
                else
                {
                    _userItems[userIndex].course = courselist[courseIndex];
                    updated = true;
                }
            }
            return updated;
        }

    }
}