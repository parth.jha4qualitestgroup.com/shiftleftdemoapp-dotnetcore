﻿using my_tdd_application_api.Interfaces;
using my_tdd_application_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace my_tdd_application_api.Services
{
    public class CourseService : ICourseService
    {
        List<Course> _courseItems;

        public CourseService()
        {
            _courseItems = new List<Course>() {
            new Course(){cId=100, courseDescription="DefaultCourse"},
            new Course() { cId=101, courseDescription="FirstCourse" }
        };
        }

        public List<Course> returnAllCourses()
        {
            return _courseItems;
        }

        public bool AddCourse(Course myCourse)
        {
            if (String.IsNullOrEmpty(myCourse.courseDescription) || myCourse.cId < 1)
            { return false; }

            var invalidCourse = false;
            for (int ci = 0; ci < _courseItems.Count; ci++) { if (_courseItems[ci].cId == myCourse.cId) { invalidCourse = true; } };
            if (invalidCourse) { return false; }
            else { _courseItems.Add(myCourse); return true; }
        }
    }
}
