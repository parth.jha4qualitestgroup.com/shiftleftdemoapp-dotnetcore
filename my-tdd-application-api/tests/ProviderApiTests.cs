using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using my_tdd_application_api;
using my_tdd_application_api.tests;
using PactNet;
using PactNet.Infrastructure.Outputters;
using System;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace tests
{
    public class ProviderApiTests : IDisposable
    {
        private string _providerUri { get; }
        private string _pactServiceUri { get; }
        private IWebHost _webHost { get; }

        private readonly ITestOutputHelper _output;

        public ProviderApiTests(ITestOutputHelper output)
        {
            _output = output;
            _providerUri = "https://localhost:5001";
            _pactServiceUri = "https://localhost:5003";

            

            _webHost = WebHost.CreateDefaultBuilder()
                .UseUrls(_pactServiceUri)
                .UseStartup<Startup>()
                .Build();

            _webHost.Start();
        }

        [Fact]
        public void TestProvider()
        {
            var config = new PactVerifierConfig
            {
                Outputters = new List<IOutput> //NOTE: We default to using a ConsoleOutput, however xUnit 2 does not capture the console output, so a custom outputter is required.
                {
                    new XUnitOutput(_output)
                },
                Verbose = true //Output verbose verification logs to the test output
            };

            IPactVerifier pactVerifier = new PactVerifier(config);
            pactVerifier.ProviderState($"{_pactServiceUri}/provider-states")
                .ServiceProvider("myTddApplication", _providerUri)
                .HonoursPactWith("myConsumer")
                .PactUri(@"..\..\..\pacts\myconsumer-mytddapplication.json")
                .Verify();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _webHost.StopAsync().GetAwaiter().GetResult();
                    _webHost.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}
