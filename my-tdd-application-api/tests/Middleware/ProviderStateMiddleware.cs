using Microsoft.AspNetCore.Http;
using my_tdd_application_api.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace my_tdd_application_api.tests.Middleware
{
    public class ProviderStateMiddleware
    {
        private const string ConsumerName = "Consumer";
        private readonly RequestDelegate _next;
        private readonly IDictionary<string, Action> _providerStates;
        private int i = 0;
        private dynamic contractFile;
        //private readonly IUserService _userService;
        public ProviderStateMiddleware(RequestDelegate next)
        {
            //_userService = userService;
            _next = next;
            _providerStates = new Dictionary<string, Action>
            {
               {
                "Invoking GetUserByUserId",
                DummyMethod
            },
                {
                "Invoking GetAllUser",
                DummyMethod
            },
                 {
                "Invoking AddUser",
                DeleteUser
            }
        };
        }



        private void DummyMethod()
        {
        }
		
		private void DeleteUser()
        {
            HttpClient _client = new HttpClient();
            _client.BaseAddress = new Uri("https://localhost:5001");
            var request = new HttpRequestMessage(HttpMethod.Get, "/api/user/1002");
            request.Headers.Add("Accept", "application/json");
            var response = _client.SendAsync(request);
            var content = JsonConvert.DeserializeObject<User>(response.Result.Content.ReadAsStringAsync().Result);
            var status = response.Result.StatusCode;
            if (content.userId == 1002)
            {
                var request1 = new HttpRequestMessage(HttpMethod.Delete, "/api/user/1002");
                request1.Headers.Add("Accept", "application/json");
                var response1 = _client.SendAsync(request1);
            }

        }



        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path.Value == "/provider-states")
            {
                this.HandleProviderStatesRequest(context);
                await context.Response.WriteAsync(String.Empty);
            }
            else
            {
                await this._next(context);
            }
        }



        private void HandleProviderStatesRequest(HttpContext context)
        {

            context.Response.StatusCode = (int)HttpStatusCode.OK;
            string jsonRequestBody = String.Empty;
            //StreamReader reader = new StreamReader(@"/builds/cadp/nasm-ct-ua/NASM-CRM-UA/pacts/nasm-crm-ua.json", Encoding.UTF8);
            StreamReader reader = new StreamReader(@"..\..\..\pacts\myconsumer-mytddapplication.json", Encoding.UTF8);
            jsonRequestBody = reader.ReadToEnd();

            contractFile = JValue.Parse(jsonRequestBody.ToString());
            while (i < contractFile.interactions.Count)
            {
                _providerStates[contractFile.interactions[i].providerState.Value].Invoke();
                i++;

            }

        }
    }
}
