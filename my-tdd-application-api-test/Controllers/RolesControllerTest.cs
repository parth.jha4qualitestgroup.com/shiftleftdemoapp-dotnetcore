﻿using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using my_tdd_application_api.Controllers;
using my_tdd_application_api.Interfaces;
using my_tdd_application_api.Models;
using NSubstitute;
using System.Collections.Generic;

namespace my_tdd_application_api_test.Controllers
{
    [TestClass]
    public class RolesControllerTest
    {
        [TestMethod]
        public void GetAllRole_returnsAllRoleList()
        {
            //Arrange
            var _iLogger = Substitute.For<ILogger<RolesController>>();
            var _roleService = Substitute.For<IRoleService>();
            var expected = new List<Roles>() { new Roles() { roleId = 101, roleName = "myFirstRoleName" } };
            _roleService.returnAllRoleList().Returns(expected);
            //Act
            RolesController rolesController = new RolesController(_iLogger, _roleService);
            var result = rolesController.GetAllRole();
            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void GetRoleByRoleId_callsreturnRoleDetailsforRoleId()
        {
            //Arrange
            var _iLogger = Substitute.For<ILogger<RolesController>>();
            var _roleService = Substitute.For<IRoleService>();
            var expected = new Roles() { roleId = 101, roleName = "myFirstRoleName" };
            _roleService.returnRoleDetailsforRoleId(Arg.Any<int>()).Returns(expected);
            //Act
            RolesController rolesController = new RolesController(_iLogger, _roleService);
            var result = rolesController.GetRoleByRoleId(101);
            //Assert
            Assert.AreEqual(expected, result);
            _roleService.Received(1).returnRoleDetailsforRoleId(Arg.Any<int>());
        }

        [TestMethod]
        public void AddRole_addsroleforvalidresponse()
        {
            //Arrange
            var _iLogger = Substitute.For<ILogger<RolesController>>();
            var _roleService = Substitute.For<IRoleService>();
            var returnData = true;
            _roleService.AddRoleToList(Arg.Any<Roles>()).Returns(returnData);
            //Act
            RolesController rolesController = new RolesController(_iLogger, _roleService);
            var myRole = new Roles();
            var result = rolesController.AddRole(myRole);
            //Assert
            Assert.AreEqual("Role is added", result);
        }

        [TestMethod]
        public void AddRole_returnserrorforinvalidresponse()
        {
            //Arrange
            var _iLogger = Substitute.For<ILogger<RolesController>>();
            var _roleService = Substitute.For<IRoleService>();
            var returnData = false;
            _roleService.AddRoleToList(Arg.Any<Roles>()).Returns(returnData);
            //Act
            RolesController rolesController = new RolesController(_iLogger, _roleService);
            var myRole = new Roles();
            var result = rolesController.AddRole(myRole);
            //Assert
            Assert.AreEqual("Invalid Role to add", result);
        }

    }
}