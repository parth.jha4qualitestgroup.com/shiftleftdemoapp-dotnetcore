﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using my_tdd_application_api.Models;
using my_tdd_application_api.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace my_tdd_application_api_test.Services
{
    [TestClass]
    public class CourseServiceTest
    {
        [TestMethod]
        public void returnAllCourses_returnsAllCourseListfromsystem()
        {
            CourseService courseService = new CourseService();
            var result = courseService.returnAllCourses();
            Assert.AreEqual(2, result.Count);
        }

        [TestMethod]
        [DataRow(101, "myNewCourseDes")]
        [DataRow(103, "")]
        [DataRow(null, "myNewCourseDes")]
        public void AddCourse_returnsfalseforinvalidCourse(int cId, string des)
        {
            CourseService courseService = new CourseService();
            var myCourse = new Course() { cId = cId, courseDescription = des };
            var result = courseService.AddCourse(myCourse);
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        [DataRow(103, "myNewCourseDes")]
        public void AddCourse_returnstrueforvalidCourse(int cId, string des)
        {
            CourseService courseService = new CourseService();
            var myCourse = new Course() { cId = cId, courseDescription = des };
            Assert.AreEqual(false, courseService.returnAllCourses().Contains(myCourse));
            var result = courseService.AddCourse(myCourse);
            Assert.AreEqual(true, result);
            Assert.AreEqual(true, courseService.returnAllCourses().Contains(myCourse));
        }
    }
}
