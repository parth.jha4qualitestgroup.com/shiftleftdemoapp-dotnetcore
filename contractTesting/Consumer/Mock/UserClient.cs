﻿using my_tdd_application_api.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;

namespace contractTesting.Consumer.Mock
{
    public class UserClient
    {
        private readonly HttpClient _client;

        public UserClient(string baseUri = null)
        {
            _client = new HttpClient { BaseAddress = new Uri(baseUri ?? "http://localhost:60881") };
        }

        public User GetUserByUserId()
        {
            string reasonPhrase;

            var request = new HttpRequestMessage(HttpMethod.Get, "/api/user/1001");
            request.Headers.Add("Accept", "application/json");            

            var response = _client.SendAsync(request);

            var content = response.Result.Content.ReadAsStringAsync().Result;
            var status = response.Result.StatusCode;

            reasonPhrase = response.Result.ReasonPhrase; //NOTE: any Pact mock provider errors will be returned here and in the response body

            request.Dispose();
            response.Dispose();

            if (status == HttpStatusCode.OK)
            {
                return !String.IsNullOrEmpty(content) ?
                  JsonConvert.DeserializeObject<User>(content)
                  : null;
            }

            throw new Exception(reasonPhrase);
        }

        public List<User> GetAllUser()
        {
            string reasonPhrase;

            var request = new HttpRequestMessage(HttpMethod.Get, "/api/user");
            request.Headers.Add("Accept", "application/json");

            var response = _client.SendAsync(request);

            var content = response.Result.Content.ReadAsStringAsync().Result;
            var status = response.Result.StatusCode;

            reasonPhrase = response.Result.ReasonPhrase; //NOTE: any Pact mock provider errors will be returned here and in the response body

            request.Dispose();
            response.Dispose();

            if (status == HttpStatusCode.OK)
            {
                return !String.IsNullOrEmpty(content) ?
                  JsonConvert.DeserializeObject<List<User>>(content)
                  : null;
            }

            throw new Exception(reasonPhrase);
        }

        public string AddUser(User myUser)
        {
            string reasonPhrase;

            var output = JsonConvert.SerializeObject(myUser);
            var stringContent = new StringContent(output, Encoding.UTF8, "application/json");

            var request = new HttpRequestMessage(HttpMethod.Post, "/api/user");
            request.Headers.Add("Accept", "application/json");
            request.Content = stringContent;

            var response = _client.SendAsync(request);

            var content = response.Result.Content.ReadAsStringAsync().Result;
            var status = response.Result.StatusCode;

            reasonPhrase = response.Result.ReasonPhrase; //NOTE: any Pact mock provider errors will be returned here and in the response body

            request.Dispose();
            response.Dispose();

            if (status == HttpStatusCode.OK)
            {
                return !String.IsNullOrEmpty(content) ?
                  content
                  : null;
            }

            throw new Exception(reasonPhrase);
        }

        public User UpdateUserRole()
        {
            string reasonPhrase;

            var body = new object();
            var output = JsonConvert.SerializeObject(body);
            var stringContent = new StringContent(output, Encoding.UTF8, "application/json");

            var request = new HttpRequestMessage(HttpMethod.Patch, "/api/user/1001&&102");           
            request.Headers.Add("Accept", "application/json");
            request.Content = stringContent;

            var response = _client.SendAsync(request);

            var content = response.Result.Content.ReadAsStringAsync().Result;
            var status = response.Result.StatusCode;

            reasonPhrase = response.Result.ReasonPhrase; //NOTE: any Pact mock provider errors will be returned here and in the response body

            request.Dispose();
            response.Dispose();

            if (status == HttpStatusCode.OK)
            {
                return !String.IsNullOrEmpty(content) ?
                  JsonConvert.DeserializeObject<User>(content)
                  : null;
            }

            throw new Exception(reasonPhrase);
        }


    }
}
